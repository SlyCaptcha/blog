from django.contrib.sitemaps import Sitemap
from .models import Post


class PostSiteMap(Sitemap):
    """
        Объект карты сайта
    """
    # атрибут changefreq - частота обновления страниц постов
    changefreq = 'weekly'
    # атрибут priorety - частота совпадения постов с тематикой (максимальное значение 1.0)
    priorety = 0.9

    def items(self):
        """
            Возвращают коллекцию опубликованных постов, которые будут отображаться на карте сайта
        """
        return Post.published.all()

    def lastmod(self, obj):
        """
            Возвращает дату и время последнего редактирования поста
        """
        return obj.updated
