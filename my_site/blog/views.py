from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from django.core.mail import send_mail
from django.db.models import Count
from django.contrib.postgres.search import (SearchVector, SearchQuery,
                                            SearchRank
                                           )

from taggit.models import Tag

from .models import Post, Comment
from .forms import EmailPostForm, CommentForm, SeachForm


# Create your views here.


class PostListView(ListView):
    queryset = Post.published.all()
    context_object_name = 'posts'
    paginate_by = 3
    template_name = 'blog/post/list.html'


# Функция не используется
def post_list(request, tag_slug=None):
    """
        Возвращает список всех опубликованных записей блога.
    """
    # posts = Post.published.all()

    # return render(
    #     request,
    #     'blog/post/list.html',
    #     {
    #         'posts': posts,
    #     }
    # )
    object_list = Post.published.all()
    tag = None

    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])

    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # Если номер страницы не является целым числом - возвращается первая страница.
        posts = paginator.page(1)
    except EmptyPage:
        # Если номер страницы больше, чем общее количество страниц - возвращается последняя страница.
        posts = paginator.page(paginator.num_pages)

    return render(
        request,
        'blog/post/list.html',
        {
            'page': page,
            'posts': posts,
            'tag': tag,
        }
    )


def post_detail(request, year, month, day, post):
    """
        Возвращяет опредленную запись по ключевому слову и дате
    """
    post = get_object_or_404(
        Post,
        slug=post,
        status='published',
        publish__year=year,
        publish__month=month,
        publish__day=day
    )
    # Обработка комментариев к посту
    comments = post.comments.filter(is_active=True)
    new_comment = None

    if request.method == 'POST':
        # Отправка пользователем комментария
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Записываем объект комментария, но сохраняем после приязвки его к посту блога
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
    else:
        comment_form = CommentForm()

    post_tag_ids = post.tags.values_list('id', flat=True)
    similar_posts = Post.published.filter(tags__in=post_tag_ids)\
                                  .exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
                                 .order_by('-same_tags', '-publish')[:4]

    return render(
        request,
        'blog/post/detail.html',
        {
            'post': post,
            'comments': comments,
            'new_comment': new_comment,
            'comment_form': comment_form,
            'similar_posts': similar_posts,
        }
    )


def email_post_share(request, post_id):
    # post = get_object_or_404(Post, id=post_id, status='published')
    post = Post.published.get(id=post_id)
    sent = False

    if request.method == 'POST':
        form = EmailPostForm(request.POST)

        if form.is_valid():
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(
                post.get_absolute_url()
            )
            subject = '{} ({}) recommends you reading "{}"'.format(
                cd['subcriber_name'],
                cd['subscriber_email'],
                post.title
                )
            message = 'Read "{}" at {}\n\n{}\'s comments: {}'.format(
                post.title,
                post_url,
                cd['subcriber_name'],
                cd['comments']
                )
            send_mail(
                subject,
                message,
                'admin@myblog.com',
                [cd['addressee_email']]
                )
            sent = True
    else:
        form = EmailPostForm()
    return render(
            request,
            'blog/post/email_share.html',
            {
                'form': form,
                'post': post,
                'sent': sent,
            }
        )


def post_search(request):
    """
        Поиск по постам
    """
    form = SeachForm()
    query = None
    result = []

    if 'query' in request.GET:
        form = SeachForm(request.GET)
    # Вариент простого поиска по заголовкам и телу постов
    # if form.is_valid():
    #     query = form.cleaned_data['query']
    #     result = Post.objects.annotate(
    #         search=SearchVector('title', 'body')
    #     ).filter(search=query)

    # Вариант поиска поиска по заголовкам и телу постов с ранжированием
    # if form.is_valid():
    #     query = form.cleaned_data['query']
    #     search_vector = SearchVector('title', 'body')
    #     search_query = SearchQuery(query)
    #     result = Post.objects.annotate(
    #         search=search_vector,
    #         rank=SearchRank(search_vector, search_query)
    #     ).filter(search=search_query).order_by('-rank')

    # Вариант поиска поиска по заголовкам и телу постов с
    # ранжированием и взвешенными запросами
    if form.is_valid():
        # Вес 'A' = 1.0, 'B' = 0.4, 'C' = 0.2, 'D' = 0.1
        # В результатах поиска выводится статьи рангом выше 0.3
        query = form.cleaned_data['query']
        search_vector = SearchVector('title', weight='A') + SearchVector('body', weight='B')
        search_query = SearchQuery(query)
        result = Post.objects.annotate(
            rank=SearchRank(search_vector, search_query)
        ).filter(rank__gte=0.3).order_by('-rank')
        print(result)

    return render(
        request,
        'blog/post/search.html',
        {
            'form': form,
            'query': query,
            'result': result,
        }
    )
