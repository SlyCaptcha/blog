"""my_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from .views import (post_list, post_detail, email_post_share,
                    post_search
                   )
from .apps import BlogConfig
from .feeds import LatestPostsFeed


app_name = BlogConfig.name

urlpatterns = [
    path('', post_list, name=f'{post_list.__name__}'),
    # path('', PostListView.as_view(), name=f'{post_list.__name__}'),
    path(
        '<int:year>/<int:month>/<int:day>/<slug:post>/',
        post_detail,
        name = f'{post_detail.__name__}'
    ),
    path(
        '<int:post_id>/share/',
        email_post_share,
        name = f'{email_post_share.__name__}'
    ),
    path(
        'tag/<slug:tag_slug>/',
        post_list,
        name = f'{post_list.__name__}_by_tag'
    ),
    path(
        'feed/', LatestPostsFeed(), name='post_feed'
    ),
    path(
        'search/', post_search, name='post_search'
    ),
]
