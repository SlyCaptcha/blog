from django.forms import (
                          Form, ModelForm, CharField,
                          EmailField, Textarea
                         )

from .models import Comment


class EmailPostForm(Form):
    """
        Форма рассылки записи блога по email
    """
    subcriber_name = CharField(max_length=25)
    subscriber_email = EmailField()
    addressee_email = EmailField()
    comments = CharField(
        required=False,
        widget=Textarea,
    )


class CommentForm(ModelForm):
    """
        Форма коммментария к посту блога
    """
    class Meta:
        model = Comment
        fields = ('name', 'email', 'body',)


class SeachForm(Form):
    """
        Форма для поиска по сайту
    """
    query = CharField()
