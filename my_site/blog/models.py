from django.contrib.auth import get_user_model
from django.db.models import (CASCADE, BooleanField, CharField,
                              DateTimeField, EmailField, ForeignKey, Manager, Model, SlugField, TextField
                             )
from django.urls import reverse
from django.utils import timezone

from taggit.managers import TaggableManager


# Create your models here.

class PublishedPostsManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='published')


class Post(Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    title = CharField(
        max_length=255,
    )
    slug = SlugField(
        max_length=255,
        unique_for_date='publish',
    )
    author = ForeignKey(
        get_user_model(),
        on_delete=CASCADE,
        related_name='blog_posts',
    )
    body = TextField()
    publish = DateTimeField(
        default=timezone.now,
    )
    created = DateTimeField(
        auto_now_add=True,
    )
    updated = DateTimeField(
        auto_now=True,
    )
    status = CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='draft',
    )
    objects = Manager()
    published = PublishedPostsManager()
    tags = TaggableManager()
    is_active = BooleanField(
        default=True,
    )

    class Meta:
        ordering = ('-publish',)
        db_table = 'blogs'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(
            'blog:post_detail',
            args=[
                self.publish.year,
                self.publish.month,
                self.publish.day,
                self.slug,
            ]
        )


class Comment(Model):
    post = ForeignKey(
        Post,
        on_delete=CASCADE,
        related_name='comments'
    )
    name = CharField(max_length=80)
    email = EmailField()
    body = TextField()
    created = DateTimeField(auto_now_add=True)
    updated = DateTimeField(auto_now=True)
    is_active = BooleanField(default=True)

    class Meta:
        ordering = ('created',)
        db_table = 'comments'

    def __str__(self):
        return 'Comment by {} on {}'.format(self.name, self.post)
